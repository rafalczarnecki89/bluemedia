<?php

use Bluemedia\Api\Item;

$factory->define(Item::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->word(),
        'amount' => 5,
    ];
});
