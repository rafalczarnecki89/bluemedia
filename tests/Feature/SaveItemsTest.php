<?php

namespace Bluemedia\Api\Tests;

use Bluemedia\Api\Item;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SaveItemsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_item_can_be_created_with_the_factory()
    {
        $item = factory(Item::class)->create();

        $this->assertCount(1, Item::all());
    }
}
