

# 1. Installing Laravel

```bash
laravel new blog

cd blog/
```

## installataion requirements: https://laravel.com/docs/7.x/installation

# 2. Install package

```bash
composer require bluemedia/api
```

# 3. Publish package config

```bash
php artisan vendor:publish --tag=bluemedia-api
```
or
```bash
php artisan vendor:publish
```
and type specific number regarding to the Tag: bluemedia-api

## The package config should appear in config directory as "bluemedia-api.php". You can define the api prefix to not collide with your application routes. Default prefix is "bluemedia".

# 4. Create database

```bash
touch database/database.sqlite
```

```bash
sudo nano .env
```
set database config to:
```bash
DB_CONNECTION=sqlite
```

instead of current configuration: (remove or comment below lines)
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

# 5. Run migrations

```bash
php artisan migrate
```

# 6. Seed data

Insert some items data.

```bash
php artisan bluemedia:seed
```

# 7. Serving Laravel
```bash
php artisan serve
```

# 8. Test Api Endpoints

## 8.1 Items available (amount > 0)
 curl -X GET http://localhost:8000/bluemedia/items/available 

 ```bash
 [
   {
      "id":1,
      "name":"Produkt 1",
      "amount":"4"
   },
   {
      "id":2,
      "name":"Produkt 2",
      "amount":"12"
   },
   {
      "id":4,
      "name":"Produkt 7",
      "amount":"6"
   },
   {
      "id":5,
      "name":"Produkt 8",
      "amount":"2"
   }
]
 ```

## 8.2 Items unavailable (amount = 0)
curl -X GET http://localhost:8000/bluemedia/items/unavailable
```bash
[
   {
      "id":3,
      "name":"Produkt 5",
      "amount":"0"
   }
]
```

## 8.3 Filter items by amount number
curl -X GET "http://localhost:8000/bluemedia/items?minAmount=5"

```bash
[
   {
      "id":2,
      "name":"Produkt 2",
      "amount":"12"
   },
   {
      "id":4,
      "name":"Produkt 7",
      "amount":"6"
   }
]
```

## 8.4 Read a resource
```bash
curl -X GET http://localhost:8000/bluemedia/item/3
```

```bash
[
   {
      "id":3,
      "name":"Produkt 5",
      "amount":"0"
   }
]
```

## 8.5 Delete a resource
```bash
curl -X DELETE http://localhost:8000/bluemedia/item/5
```

```bash
{
   "data":{
      "id":5,
      "name":"Produkt 8",
      "amount":"2"
   }
}
```

## 8.6 Create a resource
```bash
curl -X POST -H "Content-Type:application/json" -d '{"name":"Produkt 10","amount":"10"}' http://localhost:8000/bluemedia/item
```

```bash
{
   "data":{
      "id":12,
      "name":"Produkt 10",
      "amount":"10"
   }
}
```

## 8.7 Update a resource
```bash
curl -X PUT -H "Content-Type:application/json" -d '{"item_id":"12","name":"Produkt 12","amount":"12"}' http://localhost:8000/bluemedia/item
```

```bash
{
   "data":{
      "id":12,
      "name":"Produkt 12",
      "amount":"12"
   }
}
```