<?php

namespace Bluemedia\Api\Providers;

use Bluemedia\Api\Console\SeedCommand;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if($this->app->runningInConsole()){
            $this->registerPublishing();
        }
        
        $this->registerResources();
        $this->registerRoutes();
    }

    public function register()
    {
        $this->commands([
            SeedCommand::class
        ]);
    }

    private function registerResources()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
        });
    }

    /**
     * Get the Press route group configuration array.
     *
     * @return array
     */
    private function routeConfiguration()
    {
        return [
            'prefix' => config('bluemedia-api.prefix') ?? 'bluemedia',
            'namespace' => 'Bluemedia\Api\Http\Controllers'
        ];
    }

    protected function registerPublishing()
    {
        $this->publishes([
            __DIR__ . '/../../config/bluemedia-api.php' => config_path('bluemedia-api.php')
        ], 'bluemedia-api');
    }
}
