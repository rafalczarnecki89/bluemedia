<?php

namespace Bluemedia\Api\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FilterItem
 * @package Bluemedia\Api\Http\Requests
 */
class FilterItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'minAmount' => 'required|integer',
        ];
    }
}
