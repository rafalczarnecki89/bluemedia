<?php

namespace Bluemedia\Api\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreItem
 * @package Bluemedia\Api\Http\Requests
 */
class StoreItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'amount' => 'required',
            'item_id' => 'nullable|numeric|min:1',
        ];
    }
}
