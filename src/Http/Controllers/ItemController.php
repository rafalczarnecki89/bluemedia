<?php

namespace Bluemedia\Api\Http\Controllers;

use Bluemedia\Api\Http\Requests\FilterItem;
use Bluemedia\Api\Http\Requests\StoreItem;
use Illuminate\Http\JsonResponse;
use Bluemedia\Api\Item;
use Bluemedia\Api\Http\Resources\Item as ItemResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Routing\Controller;

/**
 * Class ItemController
 * @package Bluemedia\Api\Http\Controllers
 */
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param FilterItem $request
     * @return AnonymousResourceCollection
     */
    public function index(FilterItem $request): AnonymousResourceCollection
    {
        $validated = $request->validated();
        $items = Item::minAmount($validated['minAmount'])->paginate(15);

        return ItemResource::collection($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreItem $request
     * @return ItemResource
     */
    public function store(StoreItem $request): ItemResource
    {
        $validated = $request->validated();

        $item = $request->isMethod('put') ? Item::findOrFail($request->item_id) : new Item;

        $item->id = $request->input('item_id');
        $item->name = $request->input('name');
        $item->amount = $request->input('amount');

        if ($item->save()) {
            return new ItemResource($item);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return ItemResource
     */
    public function show(int $id): ItemResource
    {
        $item = Item::findOrFail($id);

        return new ItemResource($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return ItemResource
     */
    public function destroy(int $id): ItemResource
    {
        $item = Item::findOrFail($id);

        if ($item->delete()) {
            return new ItemResource($item);
        }
    }

    /**
     * @return JsonResponse
     */
    public function available(): JsonResponse
    {
        return response()->json(Item::available()->get());
    }

    /**
     * @return JsonResponse
     */
    public function unavailable(): JsonResponse
    {
        return response()->json(Item::unavailable()->get());
    }
}
