<?php


namespace Bluemedia\Api\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Class SeedCommand
 * @package Bluemedia\Api\Console
 */
class SeedCommand extends Command
{
    protected $signature = 'bluemedia:seed';
    protected $description = 'Seed items data';

    public function handle(): void
    {
        $values = [
            ['name' => 'Produkt 1', 'amount' => 4],
            ['name' => 'Produkt 2', 'amount' => 12],
            ['name' => 'Produkt 5', 'amount' => 0],
            ['name' => 'Produkt 7', 'amount' => 6],
            ['name' => 'Produkt 8', 'amount' => 2],
        ];
        DB::table('items')->insert($values);
    }
}
