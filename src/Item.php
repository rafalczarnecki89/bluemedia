<?php

namespace Bluemedia\Api;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * @package Bluemedia\Api
 * @method static available()
 * @method static unavailable()
 * @method static minAmount($minAmount)
 */
class Item extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    private const AMOUNT = 'amount';

    /**
     * Scope a query to only include available items.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public static function scopeAvailable(Builder $query): Builder
    {
        return $query->where(self::AMOUNT, '>', 0);
    }

    /**
     * Scope a query to only include unavailable items.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public static function scopeUnavailable(Builder $query): Builder
    {
        return $query->where(self::AMOUNT, 0);
    }

    /**
     * Scope a query to only include items with minimum amount.
     *
     * @param Builder $query
     * @param int $minAmount
     * @return Builder
     */
    public static function scopeMinAmount(Builder $query, int $minAmount): Builder
    {
        return $query->where(self::AMOUNT, '>=', $minAmount);
    }
}
