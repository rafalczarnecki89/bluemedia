<?php

// List articles
Route::get('items', 'ItemController@index');

// List single article
Route::get('item/{id}', 'ItemController@show');

// Create new article
Route::post('item', 'ItemController@store');

// Update article
Route::put('item', 'ItemController@store');

// Delete article
Route::delete('item/{id}', 'ItemController@destroy');

Route::get('items/available', 'ItemController@available');
Route::get('items/unavailable', 'ItemController@unavailable');

